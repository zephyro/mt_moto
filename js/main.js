$(function() {
    var pull = $('.topnav-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('open');
        pull.toggleClass('open');
    });
});

$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.closest('.cat-menu').toggleClass('open');
    });
});

$(".btn-modal").fancybox({
    'padding'    : 0,
    baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
    '<div class="fancybox-bg"></div>' +
    '<div class="fancybox-slider-wrap">' +
    '<div class="fancybox-slider fancybox-slider-main"></div>' +
    '</div>' +
    '<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
    '</div>',
    closeTpl : '<button data-fancybox-close class="modal-close"></button>'
});



$('.header-cart').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('li').toggleClass('open');
});



$('.footer-nav-title').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.footer-nav').find('.footer-nav-item').removeClass('open');
    $(this).closest('.footer-nav-item').toggleClass('open');
});


$('.filter-block-title, .filter-block-toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.filter-block').toggleClass('open');
});

$('.btn-filter-toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $('.filter').toggleClass('open');
});

$('.filter-close').on('click touchstart', function(e) {
    e.preventDefault();
    $('.filter').removeClass('open');
});

$('.filter-block-title').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.filter-block').toggleClass('open');
});

$('.btn-read').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).toggleClass('open');
    $(this).closest('.text-block').find('.text-block-toggle').toggleClass('open');
});

$('.product-detail-toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.product-detail').toggleClass('open');
});


$('.btn-view-all').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).toggleClass('open');
    $(this).closest('.shop-staff').find('.staff-list').toggleClass('open');
});

$('.btn-item-view').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).toggleClass('open');
    $(this).closest('li').toggleClass('open');
});


$('.jobs-title a').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('li').find('.jobs-content').slideToggle();
    $(this).closest('li').find('.jobs-title').toggleClass('open');
});


$('.office-history-header').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('li').toggleClass('open');
});

// Forms

$('select').selectric({
    disableOnMobile: false,
    responsive: true,
    maxHeight: 350
});



/* Поле отправки файла */

function getName (str){
    if (str.lastIndexOf('\\')){
        var i = str.lastIndexOf('\\')+1;
    }
    else{
        var i = str.lastIndexOf('/')+1;
    }
    var filename = str.slice(i);
    var uploaded = document.getElementById("fileName");
    uploaded.innerHTML = filename;
}


// Input number

$('.minus').click(function () {
    var $input = $(this).parent().find('input');
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
});
$('.plus').click(function () {
    var $input = $(this).parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
});


$('.slider-action').slick({
    dots: true,
    arrows: false,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 1
});


$('.slider-product').slick({
    dots: true,
    arrows: false,
    infinite: false,
    speed: 300,
    autoplay: false,
    autoplaySpeed: 5000,
    slidesToShow: 1
});


$('.promo').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 5,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>',
    responsive: [
        {
            breakpoint: 1210,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 640,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

$('.action').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 5,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>',
    responsive: [
        {
            breakpoint: 1210,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 640,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

$('.product-rec').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>',
    responsive: [
        {
            breakpoint: 1210,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 640,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
    ]
});

$('.product-similar').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>',
    responsive: [
        {
            breakpoint: 1210,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 640,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
    ]
});


$('.vendor').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    adaptiveHeight: true,
    autoplaySpeed: 5000,
    slidesToShow: 14,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>',
    responsive: [
        {
            breakpoint: 1210,
            settings: {
                slidesToShow: 10,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 7,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 640,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }
    ]
});


$("input[type='radio']").ionCheckRadio();
$("input[type='checkbox']").ionCheckRadio();


// Переключение каталога

$('input[name="view-top"]').change(function() {

    var viewVal = $('input[name="view-top"]:checked').val();

    switch (viewVal) {
        case 'tile':
            $(".showcase-wrap").removeClass('view-list');
            $(".showcase-wrap").removeClass('view-table');
            $('.sortable li:first-child input[name="view-bottom"]').trigger('click');
            break;
        case 'list':
            $(".showcase-wrap").removeClass('view-table');
            $(".showcase-wrap").addClass('view-list');
            $('.sortable li:nth-child(2) input[name="view-bottom"]').trigger('click');
            break;
        case 'table':
            $(".showcase-wrap").removeClass('view-list');
            $(".showcase-wrap").addClass('view-table');
            $('.sortable li:last-child input[name="view-bottom"]').trigger('click');
            break;
        default: {}
    }

});

$('input[name="view-bottom"]').change(function() {

    var viewVal = $('input[name="view-bottom"]:checked').val();

    switch (viewVal) {
        case 'tile':
            $(".showcase-wrap").removeClass('view-list');
            $(".showcase-wrap").removeClass('view-table');
            $('.sortable li:first-child input[name="view-top"]').trigger('click');
            break;
        case 'list':
            $(".showcase-wrap").removeClass('view-table');
            $(".showcase-wrap").addClass('view-list');
            $('.sortable li:nth-child(2) input[name="view-top"]').trigger('click');
            break;
        case 'table':
            $(".showcase-wrap").removeClass('view-list');
            $(".showcase-wrap").addClass('view-table');
            $('.sortable li:last-child input[name="view-top"]').trigger('click');
            break;
        default: {}
    }
});


$('.tabs-nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $('.tabs');

    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});


$('.product-thumbs a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.product-gallery').find('.product-image');

    $(this).closest('.product-thumbs').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.product-image-item').removeClass('active');
    box.find(tab).addClass('active');
});